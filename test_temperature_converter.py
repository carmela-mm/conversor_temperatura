import unittest
from app import convert_temperature

class TestTemperatureConverter(unittest.TestCase):
    def test_celsius_to_fahrenheit(self):
        self.assertAlmostEqual(convert_temperature(0, 'Celsius', 'Fahrenheit'), 32, places=2)
        self.assertAlmostEqual(convert_temperature(100, 'Celsius', 'Fahrenheit'), 212, places=2)

    def test_fahrenheit_to_celsius(self):
        self.assertAlmostEqual(convert_temperature(32, 'Fahrenheit', 'Celsius'), 0, places=2)
        self.assertAlmostEqual(convert_temperature(212, 'Fahrenheit', 'Celsius'), 100, places=2)

if __name__ == '__main__':
    unittest.main()
